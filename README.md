Demo application to demonstrate crash in MouseArea

This test application demonstrates how a delete of the QML scene
triggered from a MouseArea will crash in the Mouse Area.

To build:
        mkdir build
        cd build
        cmake ../
        make

To run:
        cd build
        ./crashDemo

Now click anywhere in the red window and it will crash with the
following stacktrace:

    #0  0x00007ffff50fd107 in __GI_raise (sig=sig@entry=6) at ../nptl/sysdeps/unix/sysv/linux/raise.c:56
    #1  0x00007ffff50fe4e8 in __GI_abort () at abort.c:89
    #2  0x00007ffff5de8291 in qt_message_fatal (context=..., message=...) at global/qlogging.cpp:1578
    #3  0x00007ffff5de495c in QMessageLogger::fatal (this=0x7fffffff8ca0, msg=0x7ffff6104270 "ASSERT: \"%s\" in file %s, line %d") at global/qlogging.cpp:781
    #4  0x00007ffff5dddb90 in qt_assert (assertion=0x7ffff67df129 "context() && engine()", file=0x7ffff67df0e4 "qml/qqmlboundsignal.cpp", line=183) at global/qglobal.cpp:2966
    #5  0x00007ffff6667fe7 in QQmlBoundSignalExpression::function (this=0xf56080) at qml/qqmlboundsignal.cpp:183
    #6  0x00007ffff6667d4c in QQmlBoundSignalExpression::sourceLocation (this=0xf56080) at qml/qqmlboundsignal.cpp:155
    #7  0x00007ffff663f1fb in QQmlData::destroyed (this=0x873d90, object=0xf54eb0) at qml/qqmlengine.cpp:1709
    #8  0x00007ffff663cf6b in QQmlData::destroyed (d=0x873d90, o=0xf54eb0) at qml/qqmlengine.cpp:674
    #9  0x00007ffff605c6e9 in QObject::~QObject (this=0xf54eb0, __in_chrg=<optimized out>) at kernel/qobject.cpp:912
    #10 0x00007ffff7265df9 in QQuickItem::~QQuickItem (this=0xf54eb0, __in_chrg=<optimized out>) at items/qquickitem.cpp:2224
    #11 0x00007ffff7324866 in QQuickMouseArea::~QQuickMouseArea (this=0xf54eb0, __in_chrg=<optimized out>) at items/qquickmousearea.cpp:439
    #12 0x00007ffff72d69c5 in QQmlPrivate::QQmlElement<QQuickMouseArea>::~QQmlElement (this=0xf54eb0, __in_chrg=<optimized out>) at ../../include/QtQml/../../src/qml/qml/qqmlprivate.h:98
    #13 0x00007ffff72d69fa in QQmlPrivate::QQmlElement<QQuickMouseArea>::~QQmlElement (this=0xf54eb0, __in_chrg=<optimized out>) at ../../include/QtQml/../../src/qml/qml/qqmlprivate.h:98
    #14 0x00007ffff605e516 in QObjectPrivate::deleteChildren (this=0xf54c90) at kernel/qobject.cpp:1946
    #15 0x00007ffff605cb80 in QObject::~QObject (this=0xf56050, __in_chrg=<optimized out>) at kernel/qobject.cpp:1024
    #16 0x00007ffff7265df9 in QQuickItem::~QQuickItem (this=0xf56050, __in_chrg=<optimized out>) at items/qquickitem.cpp:2224
    #17 0x00007ffff72c3e22 in QQuickRectangle::~QQuickRectangle (this=0xf56050, __in_chrg=<optimized out>) at items/qquickrectangle_p.h:128
    #18 0x00007ffff72d6357 in QQmlPrivate::QQmlElement<QQuickRectangle>::~QQmlElement (this=0xf56050, __in_chrg=<optimized out>) at ../../include/QtQml/../../src/qml/qml/qqmlprivate.h:98
    #19 0x00007ffff72d638c in QQmlPrivate::QQmlElement<QQuickRectangle>::~QQmlElement (this=0xf56050, __in_chrg=<optimized out>) at ../../include/QtQml/../../src/qml/qml/qqmlprivate.h:98
    #20 0x00007ffff736a976 in QQuickView::~QQuickView (this=0x6b1980, __in_chrg=<optimized out>) at items/qquickview.cpp:225
    #21 0x00007ffff736a9d2 in QQuickView::~QQuickView (this=0x6b1980, __in_chrg=<optimized out>) at items/qquickview.cpp:227
    #22 0x0000000000402200 in Helper::close() ()
    #23 0x0000000000401d35 in Helper::qt_static_metacall(QObject*, QMetaObject::Call, int, void**) ()
    #24 0x0000000000401e2e in Helper::qt_metacall(QMetaObject::Call, int, void**) ()
    #25 0x00007ffff602dd01 in QMetaObject::metacall (object=0x7fffffffdcf0, cl=QMetaObject::InvokeMetaMethod, idx=5, argv=0x7fffffff9630) at kernel/qmetaobject.cpp:296
    #26 0x00007ffff66aea54 in QQmlObjectOrGadget::metacall (this=0x7fffffff9890, type=QMetaObject::InvokeMetaMethod, index=5, argv=0x7fffffff9630) at qml/qqmlpropertycache.cpp:1689
    #27 0x00007ffff6601518 in CallMethod (object=..., index=5, returnType=43, argCount=0, argTypes=0x0, engine=0xe36bd0, callArgs=0x7fffd78010a8) at jsruntime/qv4qobjectwrapper.cpp:1173
    #28 0x00007ffff6601ef1 in CallPrecise (object=..., data=..., engine=0xe36bd0, callArgs=0x7fffd78010a8) at jsruntime/qv4qobjectwrapper.cpp:1408
    #29 0x00007ffff6604697 in QV4::QObjectMethod::callInternal (this=0x7fffd78010f8, callData=0x7fffd78010a8) at jsruntime/qv4qobjectwrapper.cpp:1893
    #30 0x00007ffff6604089 in QV4::QObjectMethod::call (m=0x7fffd78010f8, callData=0x7fffd78010a8) at jsruntime/qv4qobjectwrapper.cpp:1828
    #31 0x00007ffff64d5e14 in QV4::Object::call (this=0x7fffd78010f8, d=0x7fffd78010a8) at ../../include/QtQml/5.5.1/QtQml/private/../../../../../src/qml/jsruntime/qv4object_p.h:305
    #32 0x00007ffff6620a16 in QV4::Runtime::callProperty (engine=0xe36bd0, nameIndex=17, callData=0x7fffd78010a8) at jsruntime/qv4runtime.cpp:977
    #33 0x00007ffff660ccf6 in QV4::Moth::VME::run (this=0x7fffffffa287, engine=0xe36bd0, code=0x7fffd80431e0 "\220\377`\366\377\177", storeJumpTable=0x0) at jsruntime/qv4vme_moth.cpp:556
    #34 0x00007ffff6610656 in QV4::Moth::VME::exec (engine=0xe36bd0, code=0x7fffd8043128 "\366\311`\366\377\177") at jsruntime/qv4vme_moth.cpp:925
    #35 0x00007ffff659aeb3 in QV4::SimpleScriptFunction::call (that=0x7fffd7801000, callData=0x7fffd7801008) at jsruntime/qv4functionobject.cpp:564
    #36 0x00007ffff64d5e14 in QV4::Object::call (this=0x7fffd7801000, d=0x7fffd7801008) at ../../include/QtQml/5.5.1/QtQml/private/../../../../../src/qml/jsruntime/qv4object_p.h:305
    #37 0x00007ffff66d5cac in QQmlJavaScriptExpression::evaluate (this=0xf560a0, context=0xf55f90, function=..., callData=0x7fffd7801008, isUndefined=0x0) at qml/qqmljavascriptexpression.cpp:158
    #38 0x00007ffff6668ba0 in QQmlBoundSignalExpression::evaluate (this=0xf56080, a=0x7fffffffc270) at qml/qqmlboundsignal.cpp:281
    #39 0x00007ffff66691ed in QQmlBoundSignal_callback (e=0x7a1b28, a=0x7fffffffc270) at qml/qqmlboundsignal.cpp:408
    #40 0x00007ffff66b4d0f in QQmlNotifier::emitNotify (endpoint=0x0, a=0x7fffffffc270) at qml/qqmlnotifier.cpp:94
    #41 0x00007ffff663d3f6 in QQmlData::signalEmitted (object=0xf54eb0, index=45, a=0x7fffffffc270) at qml/qqmlengine.cpp:763
    #42 0x00007ffff606331e in QMetaObject::activate (sender=0xf54eb0, signalOffset=29, local_signal_index=16, argv=0x7fffffffc270) at kernel/qobject.cpp:3599
    #43 0x00007ffff6063120 in QMetaObject::activate (sender=0xf54eb0, m=0x7ffff75c4600 <QQuickMouseArea::staticMetaObject>, local_signal_index=16, argv=0x7fffffffc270) at kernel/qobject.cpp:3578
    #44 0x00007ffff741afd5 in QQuickMouseArea::clicked (this=0xf54eb0, _t1=0x7fffffffc2b0) at .moc/moc_qquickmousearea_p.cpp:617
    #45 0x00007ffff732711a in QQuickMouseArea::setPressed (this=0xf54eb0, button=Qt::LeftButton, p=false) at items/qquickmousearea.cpp:1193
    #46 0x00007ffff7325805 in QQuickMouseArea::mouseReleaseEvent (this=0xf54eb0, event=0x254caf0) at items/qquickmousearea.cpp:774
    #47 0x00007ffff7272cf9 in QQuickItem::event (this=0xf54eb0, ev=0x254caf0) at items/qquickitem.cpp:7289
    #48 0x00007ffff7749616 in QApplicationPrivate::notify_helper (this=0x61b210, receiver=0xf54eb0, e=0x254caf0) at kernel/qapplication.cpp:3716
    #49 0x00007ffff7746d75 in QApplication::notify (this=0x7fffffffdd10, receiver=0xf54eb0, e=0x254caf0) at kernel/qapplication.cpp:3160
    #50 0x00007ffff6024f9e in QCoreApplication::notifyInternal (this=0x7fffffffdd10, receiver=0xf54eb0, event=0x254caf0) at kernel/qcoreapplication.cpp:965
    #51 0x00007ffff7277067 in QCoreApplication::sendEvent (receiver=0xf54eb0, event=0x254caf0) at /home/martin/src/qt5/qtbase/include/QtCore/../../src/corelib/kernel/qcoreapplication.h:224
    #52 0x00007ffff728ff00 in QQuickWindow::sendEvent (this=0x6b1980, item=0xf54eb0, e=0x254caf0) at items/qquickwindow.cpp:2608
    #53 0x00007ffff728989e in QQuickWindowPrivate::deliverMouseEvent (this=0x6b5f20, event=0x7fffffffd700) at items/qquickwindow.cpp:1532
    #54 0x00007ffff7289ced in QQuickWindow::mouseReleaseEvent (this=0x6b1980, event=0x7fffffffd700) at items/qquickwindow.cpp:1572
    #55 0x00007ffff736bef4 in QQuickView::mouseReleaseEvent (this=0x6b1980, e=0x7fffffffd700) at items/qquickview.cpp:627
    #56 0x00007ffff6a95ea2 in QWindow::event (this=0x6b1980, ev=0x7fffffffd700) at kernel/qwindow.cpp:1966
    #57 0x00007ffff7289061 in QQuickWindow::event (this=0x6b1980, e=0x7fffffffd700) at items/qquickwindow.cpp:1413
    #58 0x00007ffff7749616 in QApplicationPrivate::notify_helper (this=0x61b210, receiver=0x6b1980, e=0x7fffffffd700) at kernel/qapplication.cpp:3716
    #59 0x00007ffff7746d75 in QApplication::notify (this=0x7fffffffdd10, receiver=0x6b1980, e=0x7fffffffd700) at kernel/qapplication.cpp:3160
    #60 0x00007ffff6024f9e in QCoreApplication::notifyInternal (this=0x7fffffffdd10, receiver=0x6b1980, event=0x7fffffffd700) at kernel/qcoreapplication.cpp:965
    #61 0x00007ffff6a8cee9 in QCoreApplication::sendSpontaneousEvent (receiver=0x6b1980, event=0x7fffffffd700) at ../../include/QtCore/../../src/corelib/kernel/qcoreapplication.h:227
    #62 0x00007ffff6a85ff0 in QGuiApplicationPrivate::processMouseEvent (e=0x2559870) at kernel/qguiapplication.cpp:1789
    #63 0x00007ffff6a8558a in QGuiApplicationPrivate::processWindowSystemEvent (e=0x2559870) at kernel/qguiapplication.cpp:1581
    #64 0x00007ffff6a6dc65 in QWindowSystemInterface::sendWindowSystemEvents (flags=...) at kernel/qwindowsysteminterface.cpp:625
    #65 0x00007fffeed5cd42 in userEventSourceDispatch (source=0x67e270) at eventdispatchers/qeventdispatcher_glib.cpp:70
    #66 0x00007ffff19ecfe7 in g_main_context_dispatch () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
    #67 0x00007ffff19ed240 in ?? () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
    #68 0x00007ffff19ed2ec in g_main_context_iteration () from /lib/x86_64-linux-gnu/libglib-2.0.so.0
    #69 0x00007ffff609b0d9 in QEventDispatcherGlib::processEvents (this=0x67f960, flags=...) at kernel/qeventdispatcher_glib.cpp:418
    #70 0x00007fffeed5cf66 in QPAEventDispatcherGlib::processEvents (this=0x67f960, flags=...) at eventdispatchers/qeventdispatcher_glib.cpp:115
    #71 0x00007ffff6021b5c in QEventLoop::processEvents (this=0x7fffffffdc30, flags=...) at kernel/qeventloop.cpp:128
    #72 0x00007ffff6021e50 in QEventLoop::exec (this=0x7fffffffdc30, flags=...) at kernel/qeventloop.cpp:204
    #73 0x00007ffff602567c in QCoreApplication::exec () at kernel/qcoreapplication.cpp:1229
    #74 0x00007ffff6a853ba in QGuiApplication::exec () at kernel/qguiapplication.cpp:1527
    #75 0x00007ffff774674f in QApplication::exec () at kernel/qapplication.cpp:2976
    #76 0x0000000000401cab in main ()
